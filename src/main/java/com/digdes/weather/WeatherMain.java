package com.digdes.weather;

import com.digdes.weather.conf.WeatherConfiguration;
import com.digdes.weather.service.ForecastService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author Ilya Ashikhmin (ashikhmin.i@digdes.com)
 *         Date: 10.04.17 19:14
 *         Copyright Digital Design (http://digdes.com)
 */
public class WeatherMain {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx =
                new AnnotationConfigApplicationContext(WeatherConfiguration.class);

        ForecastService forecastService = ctx.getBean(ForecastService.class);
        long spbCityId = 26063L;
        forecastService.updateForecast(spbCityId);

        forecastService.savedForecasts(spbCityId).forEach(System.out::println);
    }
}
