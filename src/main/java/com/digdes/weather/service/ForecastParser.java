package com.digdes.weather.service;

import com.digdes.weather.model.Forecast;

import java.util.List;

/**
 * ...
 *
 * @author Ilya Ashikhmin (ashikhmin.i@digdes.com)
 *         Date: 12.04.17 17:13
 *         Copyright Digital Design (http://digdes.com)
 */
public interface ForecastParser {
    List<Forecast> parse(String xml);
}
