package com.digdes.weather.service;

/**
 * ...
 *
 * @author Ilya Ashikhmin (ashikhmin.i@digdes.com)
 *         Date: 12.04.17 17:08
 *         Copyright Digital Design (http://digdes.com)
 */
public interface HttpService {
    String getUrl(String link);
}
