package com.digdes.weather.service;

import com.digdes.weather.model.Forecast;

import java.util.List;

/**
 * ...
 *
 * @author Ilya Ashikhmin (ashikhmin.i@digdes.com)
 *         Date: 12.04.17 16:49
 *         Copyright Digital Design (http://digdes.com)
 */
public interface ForecastService {
    List<Forecast> receiveForecasts(Long cityId);

    List<Forecast> savedForecasts(Long cityId);

    void updateForecast(Long cityId);
}
