package com.digdes.weather.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * ...
 *
 * @author Ilya Ashikhmin (ashikhmin.i@digdes.com)
 *         Date: 12.04.17 16:33
 *         Copyright Digital Design (http://digdes.com)
 */
@SuppressWarnings("UnusedDeclaration")
@Entity
@Data               //lombok maigic annotations. Use it carefully
@NoArgsConstructor
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long cityId;

    private Double longitude;

    private Double latitude;

    private String name;
}
